from django.shortcuts import render, get_object_or_404, redirect
from .models import Topic, Post
from .forms import NewTopicForm, PostForm
from django.contrib.auth.decorators import login_required


# Create your views here.
def forum_home(request):
    forum_topics = Topic.objects.all()

    return render(request, 'sharksblog/blogindex.html', {'topics': forum_topics})


def forum_topic_posts(request, pk):
    topics = get_object_or_404(Topic, pk=pk)

    return render(request, 'sharksblog/posts.html', {'topics': topics})


@login_required
def new_topic(request, pk):
    forum_topics = get_object_or_404(Topic, pk=pk)
    if request.method == 'POST':
        form = NewTopicForm(request.POST)
        if form.is_valid():
            topic = form.save(commit=False)
            topic.starter = request.user
            topic.save()
            Post.objects.create(
                message=form.cleaned_data.get('message'),
                topic=topic,
                created_by=request.user
            )
            return redirect('forum_topic_posts', pk=topic.pk)
    else:
        form = NewTopicForm()
    return render(request, 'sharksblog/new_topic.html', {'topics': forum_topics, 'form': form})


@login_required
def reply_forum_topic(request, pk):
    forum_topic = get_object_or_404(Topic, pk=pk)
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.topic = forum_topic
            post.created_by = request.user
            post.save()
            return redirect('forum_topic_posts', pk=pk)
    else:
        form = PostForm()
    return render(request, 'sharksblog/reply_topic.html', {'topic': forum_topic, 'form': form})