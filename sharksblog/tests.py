from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse

from .models import Post, Topic
from .views import forum_topic_posts


class TopicPostsTests(TestCase):
    def setUp(self):
        user = User.objects.create_user(username='john', email='john@doe.com', password='123')
        topic = Topic.objects.create(subject='Hello, world', starter=user)
        Post.objects.create(message='Lorem ipsum dolor sit amet', topic=topic, created_by=user)
        url = reverse('forum_topic_posts', kwargs={'pk': topic.pk})
        self.response = self.client.get(url)

    def test_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_view_function(self):
        view = resolve('/forum/topics/1/')
        self.assertEquals(view.func, forum_topic_posts)
