from django.apps import AppConfig


class SharksblogConfig(AppConfig):
    name = 'sharksblog'
