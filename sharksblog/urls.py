from django.conf.urls import url

from sharksblog import views as sharksblog_views

urlpatterns = [
    url(r'^$', sharksblog_views.forum_home, name='forum_home'),
    url(r'^topics/(?P<pk>\d+)/$', sharksblog_views.forum_topic_posts, name='forum_topic_posts'),
    url(r'^topics/(?P<pk>\d+)/new/$', sharksblog_views.new_topic, name='new_forum_topic'),
    url(r'^topics/(?P<pk>\d+)/reply/$', sharksblog_views.reply_forum_topic, name='reply_topic'),
]
