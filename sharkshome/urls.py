from django.conf.urls import url

from sharkshome import views as sharskhome_views
from sharksblog import views as sharksblog_views

urlpatterns = [
    url(r'^$', sharskhome_views.home, name='home'),
    url(r'^about_us/$', sharskhome_views.about_us, name='about_us'),
    url(r'^forum/$', sharksblog_views.forum_home, name='sharksblog'),
    url(r'^legends/$', sharskhome_views.legends, name='legends'),
]
