from django.db import models


# Create your models here.
class Player(models.Model):
    """Player models class"""
    name = models.CharField(max_length=25)
    position = models.CharField(max_length=10)
    twitter = models.CharField(max_length=25)
    fb = models.CharField(max_length=25)
    ig = models.CharField(max_length=25)
    pic = models.ImageField()

    def __str__(self):
        return self.name


class Legend(models.Model):
    """Legends model class"""
    name = models.CharField(max_length=25)
    position = models.CharField(max_length=10)
    twitter = models.CharField(max_length=25)
    fb = models.CharField(max_length=25)
    ig = models.CharField(max_length=25)
    pic = models.ImageField()

    def __str__(self):
        return self.name


class ClubPic(models.Model):
    pic = models.ImageField()


