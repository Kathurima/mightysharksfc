from django.contrib import admin
from .models import Player, Legend, ClubPic

# Register your models here.
admin.site.register(Player)
admin.site.register(Legend)
admin.site.register(ClubPic)
