from django.shortcuts import render
from .models import Player, ClubPic, Legend
from django.conf import settings
from django.views.generic import ListView


# Create your views here.
def home(request):
    players = Player.objects.all()
    clubpics = ClubPic.objects.all()

    return render(request, 'sharkshome/index.html',
                  {'players': players, 'clubpics': clubpics, 'media_url': settings.MEDIA_URL})


def legends(request):
    legendsinfo = Legend.objects.all()

    return render(request, 'sharkshome/legends.html', {'legends': legendsinfo, 'media_url': settings.MEDIA_URL})


def about_us(request):
    return render(request, 'sharkshome/about_us.html')


def blog(request):
    return render(request, 'sharksblog/blogindex.html')
